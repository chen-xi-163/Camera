import common from '@ohos.app.ability.common';
import MediaAsset from '../../common/been/MediaAsset';
import { MediaOperationAsset } from '../../common/been/MediaOperationAsset';
import { MediaSourceType } from '../../common/constants/MediaConstants';
import MediaLibOperator from './MediaLibOperator';
import fs from '@ohos.file.fs'

export default class MediaAssetBuilder {
  private context: common.Context;
  private mediaLibOperator: MediaLibOperator;
  private mediaLibAsset: MediaOperationAsset;
  private originSource;
  private realUrl;
  private displayTitle: string;
  private sourceType: MediaSourceType = MediaSourceType.DEFAULT;
  private fileDescription: number;

  constructor(context: common.Context) {
    this.context = context;
    this.mediaLibOperator = new MediaLibOperator(context);
  }

  public async setup(asset: MediaAsset) {
    this.originSource = asset.getSource();
    this.displayTitle = asset.getTitle();
    if (!isNaN(this.originSource)) {
      this.sourceType = MediaSourceType.CLIENT;
      this.mediaLibAsset = await this.mediaLibOperator.openMediaFileOperationById(this.originSource);
      this.fileDescription = this.mediaLibAsset.getFd();
      this.displayTitle = this.mediaLibAsset.getAsset().displayName;
      this.realUrl = 'fd://' + this.mediaLibAsset.getFd();
    } else if (this.originSource.startsWith('data/') || this.originSource.startsWith('/data')) {
      let fileSize = fs.statSync(this.originSource).size;
      this.sourceType = (fileSize === -1) ? MediaSourceType.DEVICE_ABSOLUTE_INCOMPLETE : MediaSourceType.DEVICE_ABSOLUTE;
      let file = fs.openSync(this.originSource);
      this.fileDescription = file.fd;
      this.realUrl = 'fd://' + this.fileDescription;


    } else if (this.originSource.indexOf('../resources/rawfile/')) {
      this.sourceType = MediaSourceType.RAWFILE;
      let rawFileName = this.originSource.substring(this.originSource.lastIndexOf('/') + 1);
      this.realUrl = await this.context.resourceManager.getRawFd(rawFileName);
    } else {
      this.realUrl = this.originSource;
    }
  }

  public getOriginSource() {
    return this.originSource;
  }

  public getSourceType(): MediaSourceType {
    return this.sourceType;
  }

  public getRealUrl() {
    return this.realUrl;
  }

  public getFileDescription() {
    return this.fileDescription;
  }

  public getDisplayTitle() {
    return this.displayTitle
  }

  public async reset() {
    switch (this.sourceType) {
      case MediaSourceType.CLIENT:
        if (this.mediaLibAsset != null) {
          await this.mediaLibOperator.closeOMediaFileOperation(this.mediaLibAsset.getAsset(), this.fileDescription)
          this.mediaLibAsset = null
        }
        break
      case MediaSourceType.DEVICE_ABSOLUTE:
      case MediaSourceType.DEVICE_ABSOLUTE_INCOMPLETE:
        fs.closeSync(this.fileDescription)
        break
      case MediaSourceType.RAWFILE:
        this.context.resourceManager.closeRawFd(this.originSource)
        break
    }
    this.sourceType = MediaSourceType.DEFAULT;
    this.fileDescription = -1;
    this.displayTitle = '';
    this.originSource = undefined;
    this.realUrl = undefined;
  }
}