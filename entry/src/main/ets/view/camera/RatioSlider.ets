import { CameraConstants } from '../../common/constants/CameraConstants';
import DimensionUtil from '../../common/utils/DimensionUtil';
import CameraService from '../../model/media/camera/CameraService';
import CustomCameraModel from '../../viewmodel/CustomCameraModel'

@Component
export default struct RatioSlider{
  private mainModel:CustomCameraModel = CustomCameraModel.getInstant();
  @State sliderBallPosX:number = 0;
  @Consume(CameraConstants.RATION_CHANGE_KEY) isRatioChanging :boolean;
  @Consume(CameraConstants.FOCUS_CHANGE_KEY) isFocusChanging:boolean;
  @Consume(CameraConstants.RATION_KEY) curRatio:number;
  aboutToAppear(){
    this.sliderBallPosX = this.mainModel.resetRatioSliderValue();
  }
  @Styles onTouchHandle(){
    .onTouch(async (event)=>{
      let focusPoint = this.mainModel.getFocusPoint(event);
      this.sliderBallPosX = Math.max(0,Math.min(this.mainModel.getRatioSliderMaxValue(),focusPoint.x));
      let change = this.curRatio +this.mainModel.getRatioSliderChangeValue(this.sliderBallPosX);
      switch (event.type){
        case TouchType.Down:
        this.mainModel.removeRationMenuHideTask();
        this.isFocusChanging = false;
        this.isRatioChanging = true;
        this.curRatio = await CameraService.getInstance().setZoomRatio(change);
        break;
        case TouchType.Move:
        this.curRatio = await CameraService.getInstance().setZoomRatio(change);
        break;
        case TouchType.Up:
        this.sliderBallPosX = this.mainModel.resetRatioSliderValue();
        this.mainModel.hideRationMenu(CameraConstants.RATIO_STEP_CHANGE_DELAY,()=>{
          this.isRatioChanging = false;
        })
        break;
        default :
        break;
      }
    })
  }
  @Builder RationControlButton(imgRes:Resource,step:number){
    Button(){
      Image(imgRes).objectFit(ImageFit.Contain)
        .width(DimensionUtil.getVp($r('app.float.ratio_step_change_size')))
        .height(DimensionUtil.getVp($r('app.float.ratio_step_change_size')))
        .onClick(async ()=>{
          this.isFocusChanging = false;
          this.isRatioChanging = true;
          this.curRatio = await CameraService.getInstance().setZoomRatio(this.curRatio +step);
          this.mainModel.hideRationMenu(CameraConstants.RATIO_STEP_CHANGE_DELAY,()=>{
            this.isRatioChanging = false;
          })
        })
    }
    .width(DimensionUtil.getVp($r('app.float.ratio_step_btn_width')))
    .height(DimensionUtil.getVp($r('app.float.ratio_step_change_size')))
    .backgroundColor($r('app.color.trans_parent'))
  }
  build(){
Row(){
  this.RationControlButton($rawfile('camera/ic_ration_reduce.png'), -CameraConstants.RATIO_CHANGE_STEP)
  Stack(){
    Row(){
      ForEach(this.mainModel.getRationBigArray(),(item,index)=>{
        Circle()
          .width(this.mainModel.getRatioStepSize())
          .height(this.mainModel.getRatioStepSize())
          .fill(Color.White)
          .opacity($r('app.float.ratio_step_opacity'))
          .offset({x:index*this.mainModel.getRatioStepOffset()})
          .visibility(this.mainModel.isRatioStepShow(this.sliderBallPosX,index) ?Visibility.Visible:Visibility.Hidden)
      },item=>item)
    }
    .width('100%')

    Text(this.isRatioChanging?CameraConstants.DEFAULT_POINT:(this.curRatio +CameraConstants.CURRENT_RATIO_UNIT))
      .height(this.mainModel.getRatioSliderBallSize())
      .width(this.mainModel.getRatioSliderBallSize())
      .borderRadius(this.mainModel.getRatioSliderBallSize()/CameraConstants.RATIO_SIZE_HALF)
      .borderWidth(DimensionUtil.getVp($r('app.float.ratio_slider_ball_border_width')))
      .borderColor(Color.White)
      .fontColor(Color.White)
      .fontWeight(FontWeight.Bold)
      .fontSize(DimensionUtil.getFp($r('app.float.ratio_slider_ball_font_size')))
      .textAlign(TextAlign.Center)
      .markAnchor({x:'50%'})
      .position({
        x:this.isRatioChanging?this.sliderBallPosX:'50%'
      })
      .scale({
        x:this.mainModel.getRatioSliderScale(this.isRatioChanging),
        y:this.mainModel.getRatioSliderScale(this.isRatioChanging)
      })
  }
  .onTouchHandle()
  .width(this.mainModel.getRatioSliderMaxValue())
  this.RationControlButton($rawfile('camera/ic_ration_addition.png'),CameraConstants.RATIO_CHANGE_STEP)
}
  }
}