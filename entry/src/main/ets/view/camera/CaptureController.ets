import { CameraConstants, CaptureMode } from '../../common/constants/CameraConstants';
import DimensionUtil from '../../common/utils/DimensionUtil';
import CameraService from '../../model/media/camera/CameraService';
import CustomCameraModel from '../../viewmodel/CustomCameraModel'
import PressureEffectButton from '../../view/PressureEffectButton';
import ModeSelector from './ModeSelector';

@Component
export default struct CaptureController {
  private viewModel: CustomCameraModel = CustomCameraModel.getInstant();
  @Consume(CameraConstants.CAPTURE_MODE_KEY) curCaptureMode: CaptureMode;
  @Consume(CameraConstants.CAPTURE_MODE_INDEX_KEY) modeSelectIndex: number;

  build() {
    Column() {
      ModeSelector().margin({
        top: DimensionUtil.getVp($r('app.float.mode_selector_interval_top')),
        bottom: DimensionUtil.getVp($r('app.float.mode_selector_interval_bottom'))
      })
      Flex({ justifyContent: FlexAlign.SpaceEvenly, alignItems: ItemAlign.Center }) {
        this.FuncButton($rawfile('camera/ic_media_lib.png'), () => {
          this.viewModel.startPicLib();
        })
        this.CaptureButton(this.curCaptureMode)
        this.FuncButton(null, () => {

        })
      }
    }.width('100%')
    .height('100%')
  }

  @Builder FuncButton(imgRes: Resource, func: () => void) {
    Button() {
      Image(imgRes).objectFit(ImageFit.Fill)
    }
    .backgroundColor($r('app.color.trans_parent'))
    .width(DimensionUtil.getVp($r('app.float.function_button_size')))
    .height(DimensionUtil.getVp($r('app.float.function_button_size')))
    .onClick(func)
  }

  @Builder CaptureButton(captureMode) {
    PressureEffectButton({
      commonSrc: captureMode === CaptureMode.VIDEO
        ? $rawfile('camera/icon_video_capture_start.png')
        : $rawfile('camera/icon_photo_capture_start.png'),
      effectSrc: captureMode === CaptureMode.VIDEO
        ? $rawfile('camera/icon_video_capture_stop.png')
        : $rawfile('camera/icon_photo_capture_start.png'),
      isEffected: CameraService.getInstance().isCapturing(),
      closer: null,
      effectCallback: (isEffected) => {
        this.viewModel.captureToggle(isEffected);
      }
    })
      .width(DimensionUtil.getVp($r('app.float.capture_button_size')))
      .height(DimensionUtil.getVp($r('app.float.capture_button_size')))
  }
}